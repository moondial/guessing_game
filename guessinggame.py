# Guessing Game

import random

print("------------------------------")
print("      M&M GUESSING GAME! ")
print("------------------------------")


print("Guess how many M&Ms are in the Jar. If you do, Beer is on the house!")
print()

mm_count = random.randint(1, 100)
attempt_limit = 5
attempts = 0


while attempts < attempt_limit:
    guess_text = input("How many M&Ms are in the jar? ")
    guess = int(guess_text)
    attempts += 1
    if mm_count == guess:
        print(f"Boom! You get a free beer. It was {guess}.")
        break
    elif guess < mm_count:
        print("Thats too LOW!")
    else:
        print("Too High!")

print(f"Later. You finished in {attempts}")

